﻿using Common.SeedWork;
using System;
using System.Net;

namespace Common.Exceptions
{
    public class AppException : Exception
    {
        public ActionResponse ActionResponse { get; set; }

        public AppException(string message) : base(message)
        {
            ActionResponse = new ActionResponse
            {
                StatusCode = HttpStatusCode.BadRequest,
                Title = message
            };
        }
    }
}
