﻿using Newtonsoft.Json;
using System.Net;

namespace Common.SeedWork
{
    public class ActionResponse
    {
        [JsonProperty("statusCode")]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
    }
}
