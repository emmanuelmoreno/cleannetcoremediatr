﻿using System.Collections.Generic;

namespace API.DataAccess.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string CountryName { get; set; }

        public virtual ICollection<Team> Teams { get; set; }
    }
}
