﻿namespace API.DataAccess.Models
{
    public  class PlayerPageList: Player
    {
        public int TotalRecords { get; set; }
    }
}
