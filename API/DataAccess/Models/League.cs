﻿using System.Collections.Generic;

namespace API.DataAccess.Models
{
    public class League
    {
        public int Id { get; set; }
        public string LeagueName { get; set; }
        public virtual ICollection<TeamLeague> TeamLeagues { get; set; }
    }
}
