﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace API.DataAccess.Models
{
    public class Coach
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TeamId { get; set; }
        public virtual Team Team { get; set; }
    }

    public class CoachEntityConfiguration : IEntityTypeConfiguration<Coach>
    {
        public void Configure(EntityTypeBuilder<Coach> builder)
        {
            builder.Property(x => x.Name).HasColumnType("nvarchar(20)");
            builder.HasOne(x => x.Team).WithOne(x => x.Coach).HasForeignKey<Coach>(x => x.TeamId);
        }
    }
}

