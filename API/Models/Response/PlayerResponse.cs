﻿namespace API.Models.Response
{
    public class PlayerResponse
    {
        public int Id { get; set; }
        public string PlayerName { get; set; }
        public string Position { get; set; }
        public int TeamId { get; set; }
    }
}
