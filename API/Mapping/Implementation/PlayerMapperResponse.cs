﻿using API.Mapping.Abstractions;
using API.Models.Response;
using API.DataAccess.Models;
using System.Collections.Generic;
using System.Linq;

namespace API.Mapping.Implementation
{
    public class PlayerMapperResponse : IMapper
    {
        public PlayerResponse ToPlayer(Player entity)
        {
            return new PlayerResponse()
            {
                Id = entity.Id,
                PlayerName = entity.PlayerName,
                Position = entity.Position,
                TeamId = entity.TeamId
            };
        }

        public Player ToEntity(PlayerResponse player)
        {
            return new Player()
            {
                Id = player.Id,
                PlayerName = player.PlayerName,
                Position = player.Position,
                TeamId = player.TeamId
            };
        }

        public IEnumerable<PlayerResponse> ToListPlayer(IEnumerable<Player> entityList)
        {
            var result = from entity in entityList
                         select new PlayerResponse()
                         {
                             Id = entity.Id,
                             PlayerName = entity.PlayerName,
                             Position = entity.Position,
                             TeamId = entity.TeamId
                         };
            return result;
        }

        public IEnumerable<Player> ToListEntity(IEnumerable<PlayerResponse> playerList)
        {
            var result = from entity in playerList
                         select new Player()
                         {
                             Id = entity.Id,
                             PlayerName = entity.PlayerName,
                             Position = entity.Position,
                             TeamId = entity.TeamId
                         };
            return result;
        }
    }
}
