﻿using API.Models.Response;
using API.DataAccess.Models;
using System.Collections.Generic;

namespace API.Mapping.Abstractions
{
    public interface IMapper
    {
        PlayerResponse ToPlayer(Player entity);
        Player ToEntity(PlayerResponse player);
        IEnumerable<PlayerResponse> ToListPlayer(IEnumerable<Player> entityList);
        IEnumerable<Player> ToListEntity(IEnumerable<PlayerResponse> playerList);
    }
}
