﻿using API.Models.Response;
using MediatR;

namespace API.UsesCases.Commands
{
    public class CreatePlayerCommand : IRequest<PlayerResponse>
    {
        public string PlayerName { get; set; }
        public string Position { get; set; }
        public int TeamId { get; set; }
    }
}
