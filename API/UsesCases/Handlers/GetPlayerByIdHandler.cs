﻿using API.Mapping.Abstractions;
using API.Models.Response;
using API.Repositories.Abstractions;
using API.UsesCases.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace API.UsesCases.Handlers
{
    public class GetPlayerByIdHandler : IRequestHandler<GetPlayerByIdQuery, PlayerResponse>
    {
        private readonly IQueryPlayerRepository _repository;
        private readonly IMapper _mapper;

        public GetPlayerByIdHandler(IQueryPlayerRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }
        public async Task<PlayerResponse> Handle(GetPlayerByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await this._repository.GetByIdAsync(request.PlayerId);
            return result is null ? null : this._mapper.ToPlayer(result);
        }
    }
}
