﻿using API.Mapping.Abstractions;
using API.Models.Response;
using API.Repositories.Abstractions;
using API.UsesCases.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace API.UsesCases.Handlers
{
    public class GetAllPlayersHandler : IRequestHandler<GetAllPlayersQuery, IEnumerable<PlayerResponse>>
    {
        private readonly IQueryPlayerRepository _repository;
        private readonly IMapper _mapper;

        public GetAllPlayersHandler(IQueryPlayerRepository repository, IMapper mapper)
        {
            this._repository = repository;
            this._mapper = mapper;
        }
        public async Task<IEnumerable<PlayerResponse>> Handle(GetAllPlayersQuery request, CancellationToken cancellationToken)
        {
            var result = await this._repository.GetAllAsync();
            return this._mapper.ToListPlayer(result);
        }
    }
}
