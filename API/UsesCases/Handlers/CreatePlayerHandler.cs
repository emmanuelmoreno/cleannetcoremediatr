﻿using API.Repositories.Abstractions;
using System.Threading.Tasks;
using MediatR;
using API.UsesCases.Commands;
using API.Models.Response;
using System.Threading;
using API.Mapping.Abstractions;
using Microsoft.Extensions.Logging;
using API.DataAccess.Models;
using System;

namespace API.UsesCases.Players
{
    public class CreatePlayerHandler : IRequestHandler<CreatePlayerCommand, PlayerResponse>
    {
        private readonly ISavingPlayerRepository _repository;
        private readonly IMapper _mapper;
        private readonly ILogger<CreatePlayerHandler> _logger;

        public CreatePlayerHandler(ISavingPlayerRepository repository, IMapper mapper, ILogger<CreatePlayerHandler> logger)
        {
            this._repository = repository;
            this._mapper = mapper;
            this._logger = logger;
        }

        public async Task<PlayerResponse> Handle(CreatePlayerCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var result = await this._repository.AddAsync(new Player() { PlayerName = request.PlayerName, Position = request.Position, TeamId = request.TeamId });
                await this._repository.SaveAsync();
#pragma warning disable IDE0071 // Simplify interpolation
                this._logger.LogInformation($"Player created with ID: {result.Id.ToString()}");
#pragma warning restore IDE0071 // Simplify interpolation
                return this._mapper.ToPlayer(result);
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex.Message);
                return this._mapper.ToPlayer(new Player { Id =0});
            }
        }
    }
}
