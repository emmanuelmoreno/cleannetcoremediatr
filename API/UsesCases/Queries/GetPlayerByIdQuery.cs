﻿using API.Models.Response;
using MediatR;

namespace API.UsesCases.Queries
{
    public record GetPlayerByIdQuery(int PlayerId) : IRequest<PlayerResponse>;
}
