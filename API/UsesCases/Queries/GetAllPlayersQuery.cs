﻿using API.Models.Response;
using MediatR;
using System.Collections.Generic;

namespace API.UsesCases.Queries
{
    public record GetAllPlayersQuery() : IRequest<IEnumerable<PlayerResponse>>;
}
