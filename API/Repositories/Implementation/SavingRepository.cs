﻿using API.Repositories.Abstractions;
using Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace API.Repositories.Implementation
{
    public class SavingRepository<T> : ISavingRepository<T> where T : class
    {
        protected readonly DbContext _context;
        protected readonly DbSet<T> _dbSet;

        public SavingRepository(DbContext context)
        {
            this._context = context;
            this._dbSet = context.Set<T>();
        }

        public async Task<T> AddAsync(T entity)
        {
            try
            {
                await this._dbSet.AddAsync(entity);
                return entity;
            }
            catch (Exception e)
            {
                throw new AppException(e.Message);
            }
        }

        public T Update(T entity)
        {
            try
            {
                this._dbSet.Attach(entity);
                this._context.Entry(entity).State = EntityState.Modified;
                return entity;
            }
            catch (Exception e)
            {
                throw new AppException(e.Message);
            }
        }

        public async Task<T> GetByIdAsync(int id)
        {
            try
            {
                var result = await this._dbSet.FindAsync(id);
                return result;
            }
            catch (Exception e)
            {
                throw new AppException(e.Message);
            }
        }

        public async Task<int> SaveAsync()
        {
            try
            {
                int result = await _context.SaveChangesAsync();
                return result;
            }
            catch (Exception e)
            {
                throw new AppException(e.Message);
            }
        }

        public async Task<T> Delete(int id)
        {
            try
            {
                T entityToDelete = await this.GetByIdAsync(id);
                this.Delete(entityToDelete);
                return entityToDelete;
            }
            catch (Exception e)
            {
                throw new AppException(e.Message);
            }
        }

        private void Delete(T entity)
        {
            try
            {
                if (this._context.Entry(entity).State == EntityState.Detached)
                {
                    this._dbSet.Attach(entity);
                }
                this._dbSet.Remove(entity);
             }
            catch (Exception e)
            {
                throw new AppException(e.Message);
            }
        }
    }
}
