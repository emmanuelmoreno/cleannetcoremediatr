﻿using API.DataAccess.Models;
using API.Repositories.Abstractions;
using Common.SeedWork;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Repositories.Implementation
{
    public class QueryPlayerRepository : QueryRepository<Player>, IQueryPlayerRepository
    {

        public QueryPlayerRepository(CommonGlobalAppSingleSettings commonGlobalAppSingleSettings) : base(commonGlobalAppSingleSettings)
        {
        }

        public Task<IEnumerable<PlayerPageList>> CustomerPagedList(int page, int rows)
        {
            throw new NotImplementedException();
        }
    }
}
