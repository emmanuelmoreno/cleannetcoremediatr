﻿using API.DataAccess.Models;
using API.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories.Implementation
{
    public class SavingPlayerRepository : SavingRepository<Player>, ISavingPlayerRepository
    {
        public SavingPlayerRepository(DbContext context) : base(context)
        {
        }
    }
}
