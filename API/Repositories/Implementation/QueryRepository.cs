﻿using API.Repositories.Abstractions;
using Common.SeedWork;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using Common.Exceptions;
using System.Linq;

namespace API.Repositories.Implementation
{
    public class QueryRepository<T> : IQueryRepository<T> where T : class
    {
        protected readonly CommonGlobalAppSingleSettings _commonGlobalAppSingleSettings;

        public QueryRepository(CommonGlobalAppSingleSettings commonGlobalAppSingleSettings)
        {
            this._commonGlobalAppSingleSettings = commonGlobalAppSingleSettings;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            try
            {
                using IDbConnection conn = new SqlConnection(this._commonGlobalAppSingleSettings.MssqlConnectionString);
                var result = await conn.GetAsync<T>(id);
                return result;
            }
            catch (Exception e)
            {
                throw new AppException(e.Message);
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            try
            {
                using IDbConnection conn = new SqlConnection(this._commonGlobalAppSingleSettings.MssqlConnectionString);
                var result = await conn.GetAllAsync<T>();
                return result.AsEnumerable();
            }
            catch (Exception e)
            {
                throw new AppException(e.Message);
            }
        }
    }
}
