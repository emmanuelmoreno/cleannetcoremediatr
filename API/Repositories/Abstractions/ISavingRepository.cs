﻿using System.Threading.Tasks;

namespace API.Repositories.Abstractions
{
    public interface ISavingRepository<T> where T : class
    {
        Task<T> AddAsync(T entity);
        T Update(T entity);
        Task<T> Delete(int id);
        Task<int> SaveAsync();
    }
}
