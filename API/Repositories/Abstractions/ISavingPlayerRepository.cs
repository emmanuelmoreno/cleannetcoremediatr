﻿using API.DataAccess.Models;

namespace API.Repositories.Abstractions
{
    public interface ISavingPlayerRepository : ISavingRepository<Player>
    {
    }
}
