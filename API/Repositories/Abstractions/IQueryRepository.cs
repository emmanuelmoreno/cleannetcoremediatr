﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Repositories.Abstractions
{
    public interface IQueryRepository<T> where T : class
    {
        Task<T> GetByIdAsync(int id);
        Task<IEnumerable<T>> GetAllAsync();
    }
}
