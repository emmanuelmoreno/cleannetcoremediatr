﻿using API.DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Repositories.Abstractions
{
    public interface IQueryPlayerRepository : IQueryRepository<Player>
    {
        Task<IEnumerable<PlayerPageList>> CustomerPagedList(int page, int rows);
    }
}
