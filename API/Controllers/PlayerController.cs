﻿using API.UsesCases.Commands;
using API.UsesCases.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PlayerController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost]
        [SwaggerOperation(Summary = "CreatePlayer")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Succesfully")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Bad Request")]
        public async Task<IActionResult> CreatePlayer([FromBody] CreatePlayerCommand command)
        {
            var result = await this._mediator.Send(command);
            return this.CreatedAtAction("CreatePlayer", new { playerId = result.Id }, result);
        }

        [HttpGet]
        [SwaggerOperation(Summary = "GetAllPlayers")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Succesfully")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Bad Request")]
        public async Task<IActionResult> GetAllPlayers()
        {
            var query = new GetAllPlayersQuery();
            var result = await this._mediator.Send(query);
            return Ok(result);
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "GetPlayerById")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Succesfully")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, "Bad Request")]
        public async Task<IActionResult> GetPlayerById(int id)
        {
            var query = new GetPlayerByIdQuery(id);
            var result = await this._mediator.Send(query);
            return (result is not null) ? Ok(result) : NotFound();
        }
    }
}
